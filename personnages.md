# Personnages joueurs

## Luc de Penthièvre, chevalier

Meneur de la troupe. La trentaine, dernier représentant d'une lignée cadette
(et pauvre) de la famille de Penthièvre. Il a été élevé à la cour de Charles de
Blois grâce à sa cousine Jeanne de Penthièvre, et a fini son éducation martiale
sous la houlette de Bertrand du Guesclin, dans l'armée duquel il sert depuis.

Origine: Chevaliers
Patron: Saint-George (chevalier) 1

Serments (mentors):
* Bertrand du Gesclin (Soldat)
* Jeanne de Penthièvre (Courtisan)

Outils:
* Hache +2
* Vêtements riches +2

Traits: 
* Motivé par la justice (+2) (Principal)
* Se révèle dans l'adversité (+2)

Thèmes:
* Combat 3
  - Guerre 4
  - Siège 4
* Etats:
  - Guerriers 2
  - Seigneurs 3
* Savoirs:
  - Langues 1
* Lieux:
  - Château 2
  - Forêt 2
* Valeurs:
  - Honneur 2

Faiblesse:
* Avarice -2

## Thomas, serviteur de Luc

Thomas est un jeune homme d'une vingtaine d'année. Il a grandi dans les rues de
Saint-Brieuc, et par un concours de circonstances que ni lui ni Luc n'abordent
publiquement, s'est retrouvé au service de Luc, affecté à des tâches
extrêmement diverses. Certains le considèrent comme son page, d'autre comme son
écuyer, d'autres comme son larbin.

Patron: Saint-Dismas (voleurs) 1
Origine: Artisans

Serments (mentors):
* Jacques l'arnaque (voleur)
* Per Kozh (forain)

Thèmes:
* Combat 1
* Lieux:
  - Ville 4
  - Château 2
* Valeurs:
  - Fortune 2
  - Amour 2
  - Foi 2
  - Corruption 2
* Etats:
  - Bourgeois 2

Trait:
* Altruiste (Principal) +2
* Les mains vives (Principal) +2

Outils:
* Cartes à jouer +2
* Passe-partout +2

Faiblesse:
* Arrogance -2

## Paul, soldat

Sixième fils d'une famille de paysan, Paul a dû s'enrôler dans la garde de son
seigneur local afin d'avoir des prospects d'avenir, ses parents n'ayant pas
assez de terres pour subvenir à tous leurs enfants. Il fit preuve de compétence
dans son nouveau métier, et lorsque son suzerain se rangea du côté des
Penthièvre dans la guerre civile, il intégra l'armée de Du Guesclin.

Patron: Saint Maurice (soldats)
Origine: Paysans

Serments (mentors):
* Bernez, chef de la guarde (garde) +2
* Olier, son père (paysan)

Traits:
* Fort comme un bœuf (Principal) +2
* Besogneux +2

Thèmes:
* Combat 1
  - Bagarre 3
  - Guerre 2
* Lieux:
  - Château 2
  - Forêt 2
  - Montagne 2
* Valeurs:
  - Foi 2
  - Honneur 2
* Etats:
  - Paysans 3

Outils:
* Lance +2
* Bottes +2

Faiblesse:
* Con comme un œuf -2

## Karadeg

Karadeg est le guide local. On ne sait pas grand-chose de lui, si ce n'est
qu'il a vécu dans la ville de Saint-Malo, qu'il semble avoir eu une éducation
poussée, et qu'il voue apparemment une haine infinie envers les Montfort.

Patron: Hermès
Origine: Clergé

Serments (mentors):
* Morgana, sa tante, magae de l'ordre hermétique (professeur, sort of) +2
* Padrig, le hors-la-loi qui l'a recueilli lorsque ses parents ont brûlé au bûcher (hors-la-loi) +2

Thèmes:
* Combat 0
* Lieux:
  - Ville 2
* Valeurs:
  - Foi 2
  - Pouvoir 2
* Etats:
  - Clercs 2
* Savoirs 3

Traits:
* Pourquoi faire simple quand on peut faire compliqué? (Principal) +2
* Magus +2

Outils:
* Grimoire "Ars Magica Hermetica" +2
* Bâton +2

Faiblesse:
* Pleutre +2
